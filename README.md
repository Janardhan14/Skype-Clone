# Skype Clone in <img src='http://sovitpoudel.com.np/wp-content/uploads/2019/01/flutter.png' height='30' width='30' align='top'>

Part -7 - Making the Chat Screen UI 

Checkout the course here : [THE CS GUY](https://youtu.be/01PUYvVoLa8)


Checkout the video for this branch [HERE](https://www.youtube.com/watch?v=h5wq7OK-cSI)


Checkout the video for this branch [HERE](https://youtu.be/FJZw-sAsGas)


Part - 3 | Implementing Google Sign In

Checkout the course here : [THE CS GUY](https://youtu.be/01PUYvVoLa8)

## Branch Details
This branch contains the code for implementing google sign in using firebase auth for the skype clone project.

Checkout the video tutorial [here](https://youtu.be/mwnUGvmSMrI)

Making a Skype Clone in Flutter - **In Progress**

Checkout the course here : [THE CS GUY](https://youtu.be/01PUYvVoLa8)

## In this branch

In this branch, we start off by making the chat list screen, or the first screen which the user interacts with as soon as the app is loaded.


This branch hosts the code where we start off with making UI in Flutter for Skype Clone App.
Checkout the video guide [here](https://www.youtube.com/watch?v=M54GufhlWPU)

## Screenshots

<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_01.png' align='left' width='30%'>
<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_02.png' align='left' width='30%'>
<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_03.png' width='30%'>

<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_04.png' align='left' width='30%'>
<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_05.png' align='left' width='30%'>
<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_06.png' width='30%'>

<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_07.png' align='left' width='30%'>
<img src='https://raw.githubusercontent.com/Ronak99/Skype-Clone/screenshots/flutter_08.png' width='30%'>




## Project Structure

[INCOMPLETE]
This project is going to be divided in several different branches. As I release a new video, the content related to that video will be committed to the branch with the same name as that of the video.


1. Firebase Integration [Branch](https://github.com/Ronak99/Skype-Clone/tree/Firebase_Integration) and [Video](https://youtu.be/P1riVXbCSAM)
2. Making ChatList Screen [Branch](https://github.com/Ronak99/Skype-Clone/tree/Part-5-Making_ChatList_Screen) and [Video](https://www.youtube.com/watch?v=kbmiyj19ph4)




1. MASTER - [Code](https://github.com/Ronak99/Skype-Clone/tree/master)

2. Firebase Integration - [Video](https://youtu.be/P1riVXbCSAM) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Firebase_Integration)

3. Google Sign In - [Video](https://youtu.be/mwnUGvmSMrI) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-3-Google_Sign_In)

4. Getting Started With UI - [Video](https://youtu.be/M54GufhlWPU) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-4-Getting_Started_With_Layout)

5. Making Chat Screen - [Video](https://youtu.be/kbmiyj19ph4) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-5-Making_ChatList_Screen)

6. Search Screen And Functionality - [Video](https://youtu.be/FJZw-sAsGas) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-6-Implementing_Search_Functionality_And_Making_SearchScreen)

7. Chat Screen UI - [Video](https://youtu.be/h5wq7OK-cSI) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-7-Making_ChatScreen_UI)

8. Exchaning Messages - [Video](https://youtu.be/27IpIJeTtwA) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-8-Exchanging_Messages_Via_Firebase)

9. Cleanup And Rearrangements - [Video](https://youtu.be/YTonb-VXdio) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-9-Cleanup_and_Rearrangement)

10. Emojis in Flutter - [Video](https://youtu.be/Hc7r62fMsTs) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-10-Emojies_In_Flutter)

11. Exchanging Images - [Video](https://youtu.be/khspvcbS7qE) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-11-Send_Images_Via_Firebase_Storage)

12. End to End Video Calling - [Video](https://youtu.be/v9ngriCV0J0) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-12-Video_Calling_Over_Network)

12.5. [BREAKING - CHANGES] - [Video](https://youtu.be/_r5bV5tr5_E) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-12-Video_Calling_Over_Network)

13. Populating Chat List Screen - [Video](https://youtu.be/Yjtax0D2MTg) - [Code](https://github.com/Ronak99/Skype-Clone/tree/Part-13-Populate_Chat_List_Screen)


